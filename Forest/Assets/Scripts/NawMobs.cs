﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;




public class NawMobs : MonoBehaviour
{
    public Transform[] areal;
    public Text mushroomMob;
    public Transform golgMushroom;
    int countMushroomMob;


    NavMeshAgent agent;
    Animator anim;
    Vector3 pointAreal;
    Transform player;
    //bool isGiveWalk = false;
    bool isGive = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        NextPointAreal();
        player = PlayerForest.player.transform;// находим игрока
        AddMushroomMob(0);
    }

    // Update is called once per frame
    void Update()
    {


        anim.SetFloat("speed", agent.velocity.magnitude);
        float distance = Vector3.Distance(transform.position, player.position);
        if (distance < 5)
        {
            if (!isGive)
            {
                Debug.DrawRay(transform.position, player.position - transform.position, Color.red);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, player.position - transform.position, out hit))
                {
                    if (hit.transform.tag == "Player")
                    {
                        isGive = true;
                        //anim.SetTrigger ("give");
                    }
                }

            }
            else
            {
                agent.SetDestination(player.position);
                
            }
        }
        else
        {
            if (isGive)
            {
                isGive = false;
                agent.SetDestination(pointAreal);
            }
        }
    }

    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            AddMushroomMob(1);
        }
    }*/

    void NextPointAreal()
    {
        pointAreal = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            pointAreal[i] = Random.Range(areal[0].position[i], areal[1].position[i]);
        }
        agent.SetDestination(pointAreal);
        StartCoroutine(StopedMob());
    }

    IEnumerator StopedMob()
    {
        yield return new WaitForSeconds(1);
        while (true)
        {
            yield return null;

            if (agent.velocity.magnitude < 0.1)
            {
                agent.SetDestination(transform.position);
                yield return new WaitForSeconds(3);
                NextPointAreal();
                break;
            }
        }


    }

    public void AddMushroomMob(int count)
    {
        countMushroomMob += count;
        mushroomMob.text = countMushroomMob.ToString();
        anim.SetTrigger("give");
        if (countMushroomMob == 3)
        {
            Instantiate(golgMushroom, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }

    }



}
