﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class PlayerForest : MonoBehaviour
{
    public float speed = 3;
    public float mouseSens = 2;
    public bool isMouseInvers = false;
    public LayerMask layerGround;
    public LayerMask layerMob;
    public static PlayerForest player;
    public Text goldMushroom;
    public Text mushroom;
    public static PlayerForest sing;
    public int countGoldMushroom;
    int countMushroom;    



    Rigidbody rb;
    Transform cam;
    Vector3 rotCam;
    Animator anim;
    bool isGround = true;
    bool isWater = false;

    public RectTransform panelTask;
    float panelTaskT;

    void Awake()
    {
        sing = this;
        player = this;
    }

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        cam = Camera.main.transform;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex==0)
        {
            AddMushroom(0);
            AddGoldMooshroom(0);
        }
        
    }
    void FixedUpdate()
    {
        isGround = Physics.Raycast(transform.position, Vector3.down, 0.2f, layerGround);
        anim.SetBool("ground", isGround);
        float move = Input.GetAxis("Vertical");
        anim.SetFloat("speed", Mathf.Abs(move));
        if(isGround)
        {
            rb.velocity = transform.TransformDirection(Input.GetAxis("Horizontal") * speed, rb.velocity.y, move * speed);
        }
        transform.Rotate(0, Input.GetAxis("Mouse X") * mouseSens, 0, Space.World);
        rotCam = cam.localEulerAngles;
        cam.Rotate(Input.GetAxis("Mouse Y") * mouseSens * (isMouseInvers ? 1 : -1), 0, 0, Space.Self);
        if ((cam.localEulerAngles.x > 310 && cam.localEulerAngles.x < 360) || (cam.localEulerAngles.x > 0 && cam.localEulerAngles.x < 30))
        { }
        else
        {
            cam.localEulerAngles = rotCam;
        }

    }


    /*private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.TransformPoint(Vector3.forward * 2), 2);        
    }*/

    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && isGround)
        {
            rb.AddForce(0, 300, 0);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            if (panelTask.position.y == panelTaskT)
            {
                panelTask.position += Vector3.up * 2000;//0,1,0 * 2000
            }
            else
            {
                panelTask.position += Vector3.down * 2000;
            }
        }
            if (Input.GetKeyDown(KeyCode.F))
        {
            Collider[] collMob = Physics.OverlapSphere(transform.TransformPoint(Vector3.forward*2), 2, layerMob);
            if (collMob.Length>0)
            {
                if (countMushroom > 0)
                {
                    AddMushroom(-1);
                    collMob[0].GetComponent<NawMobs>().AddMushroomMob(1);
                    anim.SetTrigger("pinning");

                }

            }
        }




    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Water")
        {
            isWater = true;
            anim.SetBool("Water", isWater);

        }

        


        else if (collision.transform.tag =="Bonus")
        {
            AddMushroom(1);
            Destroy(collision.gameObject);
        }

        else if (collision.transform.tag == "BonusGold")
        {
            AddGoldMooshroom(1);
            Destroy(collision.gameObject);
        }

        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Water")
        {
            isWater = false;
            anim.SetBool("Water", isWater);

        }
    }

    void AddMushroom(int count)
    {
        countMushroom += count;
        mushroom.text = countMushroom.ToString();

    }
    void AddGoldMooshroom(int count)
    {
        countGoldMushroom += count;
        goldMushroom.text = countGoldMushroom.ToString();
    }
}
