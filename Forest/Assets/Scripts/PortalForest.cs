﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalForest : MonoBehaviour
{
    public int idLoadScene;
    public Transform crater;
    bool isActiv = false;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isActiv)
        {
            crater.Rotate(0, 0, 30);
            if (Input.GetKeyDown(KeyCode.F))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(idLoadScene);
            }
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Player")
        {
            if (collision.GetComponent<PlayerForest>().countGoldMushroom == 3)
            {
                isActiv = true;
                crater.gameObject.SetActive(true);
            }
        }
    }
}
